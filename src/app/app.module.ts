import { FormsModule ,ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MachineService } from './_core/_service/machine.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HomeComponent } from './modules/general/home/home.component';
import { NotFoundComponent } from './modules/general/not-found/not-found.component';
import { AppRoutingModule } from './app-routing.module';
import { LayoutComponent } from './views/layout/layout.component';
import { SiteHeaderComponent } from './views/site-header/site-header.component';
import { SidebarComponent } from './views/sidebar/sidebar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UpdateComponent } from './modules/general/machine/update/update.component';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatSelectModule } from '@angular/material/select';
import { ToastrModule } from 'ngx-toastr';
import { AddComponent } from './modules/general/machine/add/add.component';
import { AddLocationComponent } from './modules/general/location/add-location/add-location.component';
import { MachineDetailComponent } from './modules/general/machine/machine-detail/machine-detail.component';
import { jqxGaugeModule } from 'jqwidgets-ng/jqxgauge';
import { jqxCheckBoxModule } from 'jqwidgets-ng/jqxcheckbox';
import { jqxExpanderModule } from 'jqwidgets-ng/jqxexpander';
import { jqxRadioButtonModule } from 'jqwidgets-ng/jqxradiobutton';
import { ChartsModule } from 'ng2-charts';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { MultiSelectModule } from '@syncfusion/ej2-angular-dropdowns';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
const config: SocketIoConfig = { url: 'http://10.4.2.186:3485/', options: {} };
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    LayoutComponent,
    SiteHeaderComponent,
    SidebarComponent,
    UpdateComponent,
    AddComponent,
    AddLocationComponent,
    MachineDetailComponent
  ],
  imports: [
    BsDatepickerModule.forRoot(),
    MultiSelectModule ,
    SocketIoModule.forRoot(config),
    NgxDaterangepickerMd.forRoot(),
    ChartsModule,
    jqxCheckBoxModule,
    jqxExpanderModule,
    jqxGaugeModule,
    jqxRadioButtonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatButtonToggleModule,
    MatSelectModule,
    ToastrModule.forRoot()
  ],
  providers: [MachineService],
  bootstrap: [AppComponent]
})
export class AppModule { }
