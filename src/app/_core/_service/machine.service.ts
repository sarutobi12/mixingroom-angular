import { Message } from './../_models/message';
import { Machine } from './../_models/machine';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient , HttpHeaders , HttpParams } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Socket } from 'ngx-socket-io';
const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-Type': 'application/json',
    }
  )
};
const apiUrl = 'http://10.4.4.92:92/machine';
@Injectable({
  providedIn: 'root'
})
export class MachineService {
  // baseUrl = environment.apiUrl + 'raw';
  constructor(
    private http: HttpClient,
    public socket: Socket
    ) { }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }
  getMessage() {
    return this.socket
    .fromEvent<any>('welcome')
    .pipe(map(data => data.msg));
  }
  getItems(url: string): Observable<Machine[]> {
    return this.http.get<Machine[]>(url,httpOptions)
    // .pipe(
    //   tap(cases => console.log('fetched getItems')),
    //   catchError(this.handleError('getItems', []))
    // );
  }
  pustItems(url: string , machines: Machine): Observable<any> {
    return this.http.put(url,machines,httpOptions)
    // .pipe(
    //   tap(_ => console.log(`updated machine id=${machines.machineID}`)),
    //   catchError(this.handleError<any>('updatemachine'))
    // );
  }

  addItems(url: string , machines: Machine): Observable<Machine>{
    return this.http.post<Machine>(url, machines, httpOptions).pipe(
      tap((c: Machine) => console.log(`added product w/ id=${c.machineID}`)),
      catchError(this.handleError<Machine>('addMachine'))
    );
  }

  getdatadetail(url: string){
    return this.http.get(url,httpOptions)
  }

  getrpm(url: string){
    return this.http.get(url,httpOptions)
  }

  getchart(url: string){
    return this.http.get(url,httpOptions)
  }

  download(url: string){
    return this.http.get(url,{responseType: 'blob'})
  }
}
