import { MachineService } from './../../../../_core/_service/machine.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import {Router, ActivatedRoute } from '@angular/router';

import swal from 'sweetalert2';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  machineForm: FormGroup;
  machineID = ''
  description: any
  locationID: any
  location: any
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private machineService: MachineService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.machineForm = this.formBuilder.group({
      machineID : [null, Validators.required],
      description : [null, Validators.required],
      locationID : [null, Validators.required],
    })
    this.getLocation()
  }

  onFormSubmit(){
    this.machineService.addItems('http://10.4.4.92:92/machine',this.machineForm.value)
      .subscribe((res: any)=>{
        const Toast = swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer)
            toast.addEventListener('mouseleave', swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: 'Add Machine successfully'
        })
        this.router.navigate(['']);
      })
  }

  getLocation(){
    this.machineService.getItems(`http://10.4.4.92:92/machine/${this.route.snapshot.params.id}`)
      .subscribe((res: any)=>{
        this.location = res.location
      })
  }

}
