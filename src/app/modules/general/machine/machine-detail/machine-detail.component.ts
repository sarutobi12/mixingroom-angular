import { Machine } from './../../../../_core/_models/machine';
import { Component, OnInit , ViewChild,ElementRef,ViewEncapsulation} from '@angular/core';
import { MachineService } from './../../../../_core/_service/machine.service';
import {Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { jqxGaugeComponent } from 'jqwidgets-ng/jqxgauge';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import 'chartjs-plugin-zoom';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { Socket } from 'ngx-socket-io';
import * as io from 'socket.io-client';
import { saveAs } from 'file-saver';
import swal from 'sweetalert2';

@Component({
  selector: 'app-machine-detail',
  templateUrl: './machine-detail.component.html',
  styleUrls: ['./machine-detail.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})
export class MachineDetailComponent implements OnInit {
  @ViewChild('myGauge', { static: false }) myGauge: jqxGaugeComponent;
  @ViewChild('gaugeValue', { static: false }) gaugeValue: ElementRef;
  ticksMinor: any = { interval: 5, size: '5%' };
  ticksMajor: any = { interval: 10, size: '9%' };
  labels: any = { interval: 100 };
  ranges: any[] =
  [
    {
      startValue: 0,
      endValue: 500,
      style: { fill: "red", stroke: "red" },
      endWidth: 13,
      startWidth: 13
    },
    {
      startValue: 500,
      endValue: 1000,
      style: { fill: "#4bb648", stroke: "#4bb648" },
      endWidth: 16,
      startWidth: 13
    }
  ]
  lineChartData: ChartDataSets[] = [
    { data: [], label: 'Crude oil prices' },
  ]
  lineChartLabels: Label[] = [];

  lineChartOptions = {
    responsive: true,
    maintainAspectRatio: false,
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              // min: 0
            }
          }
        ],
        xAxes: [
          {
            ticks: {
              beginAtZero: true,
              // min: 0
            }
          }
        ]
      }
  }
  lineChartOptions2 = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
            // min: 0
          }
        }
      ],
      xAxes: [
        {
          ticks: {
            beginAtZero: true,
            // min: 0
          }
        }
      ]
    },
    pan: {
      enabled: true,
      mode: "xy", 
    },
    zoom: {
      enabled: true,
      mode: "xy",
      rangeMin: {
        x: 2,
        y: 0
      },
      rangeMax: {
        x: 5,
        y: 5000
      }
    }
  }
  lineChartColors: Color[] = [
    {
      borderColor: "#08872f",
      backgroundColor: "transparent",
    },
  ];
  lineChartLegend = true
  lineChartPlugins = []
  lineChartType = 'line'
  selected: any = {start: this.route.snapshot.params.start, end: this.route.snapshot.params.end};
  model: any
  url: any = 'http://10.4.4.224:8092/raw/exportexcel/'
  historydata: any
  machineID: any
  standard: any
  minstandard: any
  location: any
  start: any = this.route.snapshot.params.start
  end: any = this.route.snapshot.params.end
  currentDate = new Date();
  form = new FormGroup({
    dateRange: new FormControl([
      new Date(),
      new Date()
    ])
  });
  bsValue = new Date();
  bsRangeValue:  Date[];
  data: Date;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private machineService: MachineService,
    private datePipe: DatePipe,
    private socket : Socket
  ) {
    this.socket = io.connect('http://10.4.2.186:3485/');
    this.bsRangeValue = [this.start, this.end];
  }
  ngOnInit(): void {
    this.getdata()
    this.linechart()
    this.machineID = this.route.snapshot.params.id
    this.socket.on('welcome', (data: any) => {
      this.machineID = data.id
      const id = this.route.snapshot.params.id
      if (this.machineID === id) {
        this.getdata()
        this.gaugechart();
        this.linechart();
      }
    })
  }

  onValueChange(bsRangeValue): void {
    let vm = bsRangeValue
    this.machineService.getchart(`http://10.4.4.224:8092/raw/detail/${this.route.snapshot.params.id}/${this.datePipe.transform(vm[0], 'yyyy-MM-dd')}/${this.datePipe.transform(vm[1], 'yyyy-MM-dd')}`)
      .subscribe((res: any)=>{
        this.router.navigate([`/machines/${this.route.snapshot.params.id}/${this.datePipe.transform(vm[0], 'yyyy-MM-dd')}/${this.datePipe.transform(vm[1], 'yyyy-MM-dd')}/detail`])
        this.historydata = res.model
        this.onValueChange2(bsRangeValue)
    })
  }
  onValueChange2(bsRangeValue): void {
    let vm = bsRangeValue
    console.log(vm)
    // console.log(this.datePipe.transform(vm[0], 'yyyy-MM-dd'))
    this.machineService.getchart(`http://10.4.4.224:8092/raw/getchart2/${this.route.snapshot.params.id}/${this.datePipe.transform(vm[0], 'yyyy-MM-dd')}/${this.datePipe.transform(vm[1], 'yyyy-MM-dd')}`)
      .subscribe((res: any)=>{
        this.lineChartData = [
          { data: res, label: 'RPM Speed' },
        ]
        res.forEach((stat) => {
          this.lineChartLabels.push(stat);
        })
    })
  }
  getdata(){
    const value = [this.route.snapshot.params.start,this.route.snapshot.params.end]
    const vm = value
    this.machineService.getdatadetail(`http://10.4.4.92:92/raw/detail/${this.route.snapshot.params.id}/${vm[0]}/${vm[1]}`)
      .subscribe((res: any)=>{
        this.standard = res.standard
        this.minstandard = res.minstandard
        this.location = res.location
        this.historydata = res.model
      })
  }
  onValueChanging(event: any): void {
    this.gaugeValue.nativeElement.innerHTML = Math.round(event.args.value) + ' RPM';
  }
  gaugechart(){
    this.machineService.getrpm(`http://10.4.4.92:92/raw/getrpm/${this.route.snapshot.params.id}`)
      .subscribe((res: any)=>{
        // this.myGauge.value = res.rpm
        this.myGauge.value(res.rpm)
      })
  }
  linechart(){
    this.machineService.getchart(`http://10.4.4.92:92/raw/getchart2/${this.route.snapshot.params.id}/${this.start}/${this.end}`)
      .subscribe((res: any)=>{
        this.lineChartData = [
          { data: res, label: 'RPM Speed' },
        ]
        res.forEach((stat) => {
          this.lineChartLabels.push(stat);
        })
      })
  }
  tableRowClassName(item){
    if (item.rpm < this.standard && item.rpm > this.minstandard ) {
      return 'table-success'
    } else  {
      return 'table-danger'
    }
  }
  // ngModelChange(selected){
  //   this.machineService.getchart(`http://10.4.4.224:8092/raw/detail/${this.route.snapshot.params.id}/${this.datePipe.transform(selected.start, 'yyyy-MM-dd')}/${this.datePipe.transform(selected.end, 'yyyy-MM-dd')}`)
  //     .subscribe((res: any)=>{
  //       // console.log(res)
  //       this.router.navigate([`/machines/${this.route.snapshot.params.id}/${this.datePipe.transform(selected.start, 'yyyy-MM-dd')}/${this.datePipe.transform(selected.end, 'yyyy-MM-dd')}/detail`])
  //       this.historydata = res.model
  //       this.ngModelChange2(selected)
  //   })
  // }
  // ngModelChange2(selected){
  //   this.machineService.getchart(`http://10.4.4.224:8092/raw/getchart2/${this.route.snapshot.params.id}/${this.datePipe.transform(selected.start, 'yyyy-MM-dd')}/${this.datePipe.transform(selected.end, 'yyyy-MM-dd')}`)
  //     .subscribe((res: any)=>{
  //       // console.log(res)
  //       this.lineChartData = [
  //         { data: res, label: 'RPM Speed' },
  //       ]
  //       res.forEach((stat) => {
  //         this.lineChartLabels.push(stat);
  //       })
  //   })
  // }
  exportExcel(){
    // this.machineService.download(`http://10.4.4.224:8092/raw/exportexcel/${this.machineID}/${this.start}/${this.end}`)
    //   .subscribe((res: any)=>{
    //     this.forceFileDownload(res)
    //     console.log('dowload success')
    //   })
    this.machineService.download(`http://10.4.4.224:8092/raw/exportexcel/${this.machineID}/${this.start}/${this.end}`)
      .subscribe(data =>{
        (saveAs(data,'Data.xlsx'))
        const Toast = swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer)
            toast.addEventListener('mouseleave', swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: 'Download successfully'
        })
      })
  }
}
