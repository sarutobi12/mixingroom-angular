import { Machine } from './../../../../_core/_models/machine';
import { MachineService } from './../../../../_core/_service/machine.service';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import {Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ErrorStateMatcher } from '@angular/material/core';
import swal from 'sweetalert2';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  machineForm: FormGroup;
  machineID = '';
  description: any
  locationname = '';
  location: any
  locationID: any
  machines: Machine = { machineID: '', description: '', locationID: ''};
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private machineService: MachineService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getMachineID()
    this.machineForm = this.formBuilder.group({
      machineID : [null, Validators.required],
      description : [null, Validators.required],
      locationID : [null, Validators.required],
    });
  }
  getMachineID(){
    this.machineService.getItems(`http://10.4.4.92:92/machine/${this.route.snapshot.params.id}`)
    .subscribe((r:any)=>{
      this.machineID = r.data.machineID
      this.machines = r.data
      this.location = r.location
      this.description = r.data.description
      this.locationname = r.location.locationname
      this.locationID = r.data.locationID
    })
  }
  onFormSubmit() {
    this.machineService.pustItems('http://10.4.4.92:92/machine',this.machineForm.value)
      .subscribe((res: any)=>{
        const Toast = swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
          onOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer)
            toast.addEventListener('mouseleave', swal.resumeTimer)
          }
        })
        Toast.fire({
          icon: 'success',
          title: 'Update Machine successfully'
        })
        this.router.navigate(['']);
      })
  }
}
