import { Machine } from './../../../_core/_models/machine';
import { MachineService } from './../../../_core/_service/machine.service';
import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute } from '@angular/router';
import { DatePipe } from '@angular/common';
// import { environment } from '../../../../environments/environment';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [DatePipe]
})
export class HomeComponent implements OnInit {
  items: any;
  data: Machine[] = [];
  loaded: boolean;
  myDate = new Date();
  start: any
  end: any
  constructor(
    private datePipe: DatePipe,
    private machineService: MachineService,
    private router: Router,
    private route: ActivatedRoute
  ) {  }

  ngOnInit() {
    this.getall();
  }
  getall(){
    this.machineService.getItems('http://10.4.4.92:92/raw')
    .subscribe(
      (items: any) => {
        this.data = items;
        this.loaded = true;
      });
  }
  getRoute(machineID){
    this.start  = this.datePipe.transform(Date.now(), 'yyyy-MM-dd')
    this.end = this.datePipe.transform(Date.now(), 'yyyy-MM-dd')
    return this.router.navigate([`/machines/${machineID}/${this.start}/${this.end}/detail`]);
  }
}