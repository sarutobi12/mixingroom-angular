import { MachineDetailComponent } from './modules/general/machine/machine-detail/machine-detail.component';
import { AddLocationComponent } from './modules/general/location/add-location/add-location.component';
import { AddComponent } from './modules/general/machine/add/add.component';
import { UpdateComponent } from './modules/general/machine/update/update.component';
import { LayoutComponent } from './views/layout/layout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './modules/general/home/home.component';
import { NotFoundComponent } from './modules/general/not-found/not-found.component';
const routes: Routes = [
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  { path: 'home',
    component: LayoutComponent,
    children: [
      { path: '', component: HomeComponent}
    ]
  },
  { path: 'machines/:id',
    component: LayoutComponent,
    children: [
      { path: '', component: UpdateComponent}
    ]
  },
  { path: 'add-machine',
    component: LayoutComponent,
    children: [
      { path: '', component: AddComponent}
    ]
  },
  { path: 'add-location',
    component: LayoutComponent,
    children: [
      { path: '', component: AddLocationComponent}
    ]
  },
  { path: 'machines/:id/:start/:end/detail',
    component: LayoutComponent,
    children: [
      { path: '', component: MachineDetailComponent}
    ]
  },
  // {
  //   path: 'signin',
  //   loadChildren: () => import('./modules/general/signin/signin.module')
  //     .then(mod => mod.SigninModule)
  // },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }